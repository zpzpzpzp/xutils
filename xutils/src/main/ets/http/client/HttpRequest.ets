/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {HttpMethod} from './HttpMethod'
import {RequestParams} from '../RequestParams'
import {RequestCallBackHandler} from '../callback/RequestCallBackHandler'
import {URIBuilder} from './util/URIBuilder'

export class HttpRequest {
  private method: HttpMethod ;
  private uriBuilder: URIBuilder;

  constructor(method: HttpMethod, uri: string) {
    this.method = method
    this.setURI(uri);
  }

  public addHeaders(nameValuePairs: Map<string, any>): HttpRequest{
    if (nameValuePairs != null) {
      this.uriBuilder.addHeaders(nameValuePairs);
    }
    return this;
  }

  public addQueryStringParams(nameValuePairs: Map<string, any>): HttpRequest{
    if (nameValuePairs != null) {
      this.uriBuilder.addParameter(nameValuePairs);
    }
    return this;
  }

  public setRequestParams(param: RequestParams, callBackHandler: RequestCallBackHandler) {
    if (param != null) {
      this.addHeaders(param.getHeaders());
      this.addQueryStringParams(param.getQueryStringParams());
    }
  }

  public getURIBuilder(): URIBuilder {
    return this.uriBuilder;
  }

  public setURI(uri: string) {
    this.uriBuilder = new URIBuilder(uri);
  }

  public getMethod(): string {
    return this.method.valueOf();
  }
}
