/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import fileio from '@ohos.fileio';
import { LruDiskCacheBean } from './LruDiskCacheBean'
import ability_featureAbility from '@ohos.ability.featureAbility';

export class LruDiskCache {
  FileName = "xBitmapCache";
  AppVersion: string = "1.0.0"
  MaxValue: number = 50 * 1024
  StorageSize: number = 0;
  IsDeleteAll: boolean = false
  mFilePath: string = "";
  constructor(fileName?: string, appVersion?: string, maxValue?: number) {
    if (fileName != null) {
      this.FileName = fileName
    }
    if (appVersion != null) {
      this.AppVersion = appVersion
    }
    if (maxValue >= 0) {
      this.MaxValue = maxValue
    }

    this.initDiskCache()
  }

  private writeText(value: string) {

    ability_featureAbility.getContext().getFilesDir().then((dir) => {
      let fd = fileio.openSync(dir + "/" + this.FileName + "/journal.txt", 0o102, 0o666);
      fileio.writeSync(fd, value, { offset: 0, position: 0, encoding: 'utf-8' });
    })
  }

  private clearText() {

    ability_featureAbility.getContext().getFilesDir().then((dir) => {
      fileio.unlinkSync(dir + "/" + this.FileName + "/journal.txt")
    })
  }

  //初始化文件
  private initDiskCache() {
    ability_featureAbility.getContext().getFilesDir().then((dir) => {
      this.mFilePath = dir
      try {
        fileio.accessSync(dir + "/" + this.FileName + "/journal.txt")
      } catch (e) {
        this.writeText('{"title": "libcore.io.DiskLruCache","cacheVersion": "1","appVersion": "' + this.AppVersion + '","maxValue": ' + this.MaxValue + ',"storageSize":' + this.StorageSize + ',"imageKey":[]}')
      }

    })

  }

  private getJournal(): string {
    let Filepath = this.mFilePath + "/" + this.FileName + "/journal.txt";
    let decodedString = fileio.readTextSync(Filepath)

    return decodedString;
  }

  public getAllImageName(): LruDiskCacheBean{

    let mDefaultLruDiskCacheBean: LruDiskCacheBean

    let Filepath = this.mFilePath + this.FileName + "/journal.txt";

    let decodedString = fileio.readTextSync(Filepath)

    mDefaultLruDiskCacheBean = new LruDiskCacheBean(decodedString);

    return mDefaultLruDiskCacheBean
  }

  public addImageItem(ImageName: string, ImageSize: number) {
    let CacheBean = this.getJournal();
    let mDefaultLruDiskCacheBean = new LruDiskCacheBean(CacheBean);
    mDefaultLruDiskCacheBean.setImageKey(ImageName);
    mDefaultLruDiskCacheBean.setStorageSize((mDefaultLruDiskCacheBean.getStorageSize() + ImageSize))
    let newValue = mDefaultLruDiskCacheBean.toJson();
    this.writeText(newValue);

  }

  public DeleteItemImage(ImageName: string, ImageSize: number) {

    let CacheBean = this.getJournal();
    let mDefaultLruDiskCacheBean = new LruDiskCacheBean(CacheBean);
    fileio.unlinkSync(this.mFilePath + "/" + ImageName);
    mDefaultLruDiskCacheBean.deleteItemImageKey(ImageName);

    mDefaultLruDiskCacheBean.setStorageSize((mDefaultLruDiskCacheBean.getStorageSize() - ImageSize))
    let newValue = mDefaultLruDiskCacheBean.toJson();
    this.clearText();
    this.writeText(newValue)
  }

  public DeleteAllImage(isDeleteAll: boolean, path: string, isCache: boolean) {
    let mDefaultLruDiskCacheBean: LruDiskCacheBean
    if (isDeleteAll) {
      let readText = this.getJournal();
      mDefaultLruDiskCacheBean = new LruDiskCacheBean(readText);
      if (mDefaultLruDiskCacheBean.getImageKey().length >= 0) {
        mDefaultLruDiskCacheBean.getImageKey().forEach((val, idx) => {
          try {
            fileio.unlinkSync(path + val)
          } catch (e) {
            throw new Error("File deletion exception")
          }

        })
      }

      mDefaultLruDiskCacheBean.deleteAllImageKey()
      mDefaultLruDiskCacheBean.setStorageSize(0)


      let newValue = mDefaultLruDiskCacheBean.toJson();
      if (!isCache) {
        this.clearText();
        this.writeText(newValue)
      }


    }

  }
}